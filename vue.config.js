let GlobalURL='http://127.0.0.1:9090/'; //'http://127.0.0.1:9090/'; 'http://43.139.80.188:9090/';//通用网关链接
module.exports = {
	//设置为空打包后不分更目录还是多级目录
	publicPath:'',
	//build编译后存放静态文件的目录
	//assetsDir: "static",

	// build编译后不生成资源MAP文件
	productionSourceMap: false,

	//开发服务,build后的生产模式还需nginx代理
	devServer: {
		open: false, //运行后自动打开浏览器
		port: 2900, //挂载端口
		proxy: {
			'/manage/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/payment/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/workflow/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/customer/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/content/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/administration/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/task/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/doc/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/auth/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/shopping/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/pay/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/finance/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/product/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/sales/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/seller/api/': {target: GlobalURL,ws: true,changeOrigin: true},
			'/purchase/api/': {target: GlobalURL,ws: true,changeOrigin: true},
		}
	},

	chainWebpack: config => {
		// 移除 prefetch 插件
		config.plugins.delete('preload');
		config.plugins.delete('prefetch');
		config.resolve.alias.set('vue-i18n', 'vue-i18n/dist/vue-i18n.cjs.js');
	},

	configureWebpack: config => {
		//性能提示
		config.performance = {
			hints: false
		}
		config.optimization = {
			splitChunks: {
				chunks: "all",
				automaticNameDelimiter: '~',
				name: true,
				cacheGroups: {
					//第三方库抽离
					vendor: {
						name: "modules",
						test: /[\\/]node_modules[\\/]/,
						priority: -10
					},
					elicons: {
						name: "elicons",
						test: /[\\/]node_modules[\\/]@element-plus[\\/]icons[\\/]/
					},
					tinymce: {
						name: "tinymce",
						test: /[\\/]node_modules[\\/]tinymce[\\/]/
					},
					echarts: {
						name: "echarts",
						test: /[\\/]node_modules[\\/]echarts[\\/]/
					},
					xgplayer: {
						name: "xgplayer",
						test: /[\\/]node_modules[\\/]xgplayer.*[\\/]/
					}
				}
			}
		}
	}

}
