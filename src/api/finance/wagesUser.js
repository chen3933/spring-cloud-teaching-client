import config from "@/config"
import http from "@/utils/request"
export default {
	wagesUser: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/wagesUser/list`,
			name: "获取个人薪资记录详情，用mongoDB保存",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/wagesUser/save`,
			name: "保存个人薪资记录详情，用mongoDB保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/wagesUser/delete`,
            name: "删除个人薪资记录详情，用mongoDB保存",
            delete: async function(wagesUserId){
                return await http.delete(this.url+"/"+wagesUserId, null);
            }
        },
	},
}