import config from "@/config"
import http from "@/utils/request"
export default {
	billOnline: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/billOnline/list`,
			name: "获取资金流-历史记录处理,线上,,主键由bill账单表生成",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/billOnline/save`,
			name: "保存资金流-历史记录处理,线上,,主键由bill账单表生成",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/billOnline/delete`,
            name: "删除资金流-历史记录处理,线上,,主键由bill账单表生成",
            delete: async function(billOnlineId){
                return await http.delete(this.url+"/"+billOnlineId, null);
            }
        },
	},
}