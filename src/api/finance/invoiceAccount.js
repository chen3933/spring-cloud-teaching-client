import config from "@/config"
import http from "@/utils/request"
export default {
	invoiceAccount: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/invoiceAccount/list`,
			name: "获取Invoicing account 开票账户，客户开票账户 和 企业内部开票账户，公用同一个表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/invoiceAccount/save`,
			name: "保存Invoicing account 开票账户，客户开票账户 和 企业内部开票账户，公用同一个表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/invoiceAccount/delete`,
            name: "删除Invoicing account 开票账户，客户开票账户 和 企业内部开票账户，公用同一个表",
            delete: async function(invoiceAccountId){
                return await http.delete(this.url+"/"+invoiceAccountId, null);
            }
        },
	},
}