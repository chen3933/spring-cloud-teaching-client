import config from "@/config"
import http from "@/utils/request"
export default {
	financeAccount: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/financeAccount/list`,
			name: "获取资金账户列表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/financeAccount/save`,
			name: "保存资金账户",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/financeAccount/delete`,
            name: "删除资金账户",
            delete: async function(financeAccountId){
                return await http.delete(this.url+"/"+financeAccountId, null);
            }
        },
	},
}
