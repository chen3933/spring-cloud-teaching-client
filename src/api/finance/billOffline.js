import config from "@/config"
import http from "@/utils/request"
export default {
	billOffline: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/billOffline/list`,
			name: "获取资金流-历史记录处理,,主键由bill账单表生成",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/billOffline/save`,
			name: "保存资金流-历史记录处理,,主键由bill账单表生成",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/billOffline/delete`,
            name: "删除资金流-历史记录处理,,主键由bill账单表生成",
            delete: async function(billOfflineId){
                return await http.delete(this.url+"/"+billOfflineId, null);
            }
        },
	},
}