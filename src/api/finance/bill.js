import config from "@/config"
import http from "@/utils/request"
export default {
	bill: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/bill/list`,
			name: "获取资金流-业务关联处理",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/bill/save`,
			name: "保存资金流-业务关联处理",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/bill/delete`,
            name: "删除资金流-业务关联处理",
            delete: async function(billId){
                return await http.delete(this.url+"/"+billId, null);
            }
        },
	},
}