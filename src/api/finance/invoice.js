import config from "@/config"
import http from "@/utils/request"
export default {
	invoice: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/invoice/list`,
			name: "获取发票，开票记录",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/invoice/save`,
			name: "保存发票，开票记录",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/invoice/delete`,
            name: "删除发票，开票记录",
            delete: async function(invoiceId){
                return await http.delete(this.url+"/"+invoiceId, null);
            }
        },
	},
}