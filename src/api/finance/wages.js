import config from "@/config"
import http from "@/utils/request"
export default {
	wages: {
		list: {
			url: `${config.API_URL_FINANCE}/finance/wages/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_FINANCE}/finance/wages/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_FINANCE}/finance/wages/delete`,
            name: "删除",
            delete: async function(wagesId){
                return await http.delete(this.url+"/"+wagesId, null);
            }
        },
	},
}