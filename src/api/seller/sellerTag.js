import config from "@/config"
import http from "@/utils/request"
export default {
	sellerTag: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerTag/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerTag/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerTag/delete`,
            name: "删除",
            delete: async function(sellerTagId){
                return await http.delete(this.url+"/"+sellerTagId, null);
            }
        },
	},
}