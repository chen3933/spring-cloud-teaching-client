import config from "@/config"
import http from "@/utils/request"
export default {
	sellerCommentImg: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerCommentImg/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerCommentImg/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerCommentImg/delete`,
            name: "删除",
            delete: async function(sellerCommentImgId){
                return await http.delete(this.url+"/"+sellerCommentImgId, null);
            }
        },
	},
}