import config from "@/config"
import http from "@/utils/request"
export default {
	sellerDevelopment: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerDevelopment/list`,
			name: "获取商家拓展",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerDevelopment/save`,
			name: "保存商家拓展",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerDevelopment/delete`,
            name: "删除商家拓展",
            delete: async function(sellerDevelopmentId){
                return await http.delete(this.url+"/"+sellerDevelopmentId, null);
            }
        },
	},
}