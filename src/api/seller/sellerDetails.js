import config from "@/config"
import http from "@/utils/request"
export default {
	sellerDetails: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerDetails/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerDetails/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerDetails/delete`,
            name: "删除",
            delete: async function(sellerDetailsId){
                return await http.delete(this.url+"/"+sellerDetailsId, null);
            }
        },
	},
}