import config from "@/config"
import http from "@/utils/request"
export default {
	sellerCheckData: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerCheckData/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerCheckData/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerCheckData/delete`,
            name: "删除",
            delete: async function(sellerCheckDataId){
                return await http.delete(this.url+"/"+sellerCheckDataId, null);
            }
        },
	},
}