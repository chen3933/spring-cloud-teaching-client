import config from "@/config"
import http from "@/utils/request"
export default {
	sellerDo: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerDo/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerDo/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerDo/delete`,
            name: "删除",
            delete: async function(sellerDoId){
                return await http.delete(this.url+"/"+sellerDoId, null);
            }
        },
	},
}