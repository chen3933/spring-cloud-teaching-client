import config from "@/config"
import http from "@/utils/request"
export default {
	sellerComment: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerComment/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerComment/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerComment/delete`,
            name: "删除",
            delete: async function(sellerCommentId){
                return await http.delete(this.url+"/"+sellerCommentId, null);
            }
        },
	},
}