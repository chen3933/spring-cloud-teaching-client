import config from "@/config"
import http from "@/utils/request"
export default {
	sellerWithdrawal: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerWithdrawal/list`,
			name: "获取提现申请，还需完善",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerWithdrawal/save`,
			name: "保存提现申请，还需完善",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerWithdrawal/delete`,
            name: "删除提现申请，还需完善",
            delete: async function(sellerWithdrawalId){
                return await http.delete(this.url+"/"+sellerWithdrawalId, null);
            }
        },
	},
}