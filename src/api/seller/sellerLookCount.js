import config from "@/config"
import http from "@/utils/request"
export default {
	sellerLookCount: {
		list: {
			url: `${config.API_URL_SELLER}/seller/sellerLookCount/list`,
			name: "获取按天数统计mongoDB记录详情",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SELLER}/seller/sellerLookCount/save`,
			name: "保存按天数统计mongoDB记录详情",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SELLER}/seller/sellerLookCount/delete`,
            name: "删除按天数统计mongoDB记录详情",
            delete: async function(sellerLookCountId){
                return await http.delete(this.url+"/"+sellerLookCountId, null);
            }
        },
	},
}