import config from "@/config"
import http from "@/utils/request"
export default {
	customerGold: {
		list: {
			url: `${config.API_URL_CUSTOMER}/customer/customerGold/list`,
			name: "获取金币消费记录表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CUSTOMER}/customer/customerGold/save`,
			name: "保存金币消费记录表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CUSTOMER}/customer/customerGold/delete`,
            name: "删除金币消费记录表",
            delete: async function(customerGoldId){
                return await http.delete(this.url+"/"+customerGoldId, null);
            }
        },
	},
}