import config from "@/config"
import http from "@/utils/request"
export default {
	customerIntegral: {
		list: {
			url: `${config.API_URL_CUSTOMER}/customer/customerIntegral/list`,
			name: "获取用户积分表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CUSTOMER}/customer/customerIntegral/save`,
			name: "保存用户积分表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CUSTOMER}/customer/customerIntegral/delete`,
            name: "删除用户积分表",
            delete: async function(customerIntegralId){
                return await http.delete(this.url+"/"+customerIntegralId, null);
            }
        },
	},
}