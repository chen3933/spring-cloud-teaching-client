import config from "@/config"
import http from "@/utils/request"
export default {
	customer: {
		list: {
			url: `${config.API_URL_CUSTOMER}/customer/customer/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CUSTOMER}/customer/customer/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CUSTOMER}/customer/customer/delete`,
            name: "删除",
            delete: async function(customerId){
                return await http.delete(this.url+"/"+customerId, null);
            }
        },
	},
}