import config from "@/config"
import http from "@/utils/request"
export default {
	membership: {
		list: {
			url: `${config.API_URL_CUSTOMER}/customer/membership/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CUSTOMER}/customer/membership/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CUSTOMER}/customer/membership/delete`,
            name: "删除",
            delete: async function(membershipId){
                return await http.delete(this.url+"/"+membershipId, null);
            }
        },
	},
}