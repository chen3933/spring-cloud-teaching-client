import config from "@/config"
import http from "@/utils/request"
export default {
	customerAddress: {
		list: {
			url: `${config.API_URL_CUSTOMER}/customer/customerAddress/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CUSTOMER}/customer/customerAddress/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CUSTOMER}/customer/customerAddress/delete`,
            name: "删除",
            delete: async function(customerAddressId){
                return await http.delete(this.url+"/"+customerAddressId, null);
            }
        },
	},
}