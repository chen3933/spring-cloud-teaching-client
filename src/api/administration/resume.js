import config from "@/config"
import http from "@/utils/request"
export default {
	resume: {
		list: {
			url: `${config.API_URL_ADMINISTRATION}/administration/resume/list`,
			name: "获取求职简历",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_ADMINISTRATION}/administration/resume/save`,
			name: "保存求职简历",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_ADMINISTRATION}/administration/resume/delete`,
            name: "删除求职简历",
            delete: async function(resumeId){
                return await http.delete(this.url+"/"+resumeId, null);
            }
        },
	},
}