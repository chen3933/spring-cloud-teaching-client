import config from "@/config"
import http from "@/utils/request"
export default {
	supplierType: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierType/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierType/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/supplierType/delete`,
            name: "删除",
            delete: async function(supplierTypeId){
                return await http.delete(this.url+"/"+supplierTypeId, null);
            }
        },
	},
}