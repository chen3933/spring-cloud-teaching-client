import config from "@/config"
import http from "@/utils/request"
export default {
	purchaseOrderItem: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/purchaseOrderItem/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/purchaseOrderItem/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/purchaseOrderItem/delete`,
            name: "删除",
            delete: async function(purchaseOrderItemId){
                return await http.delete(this.url+"/"+purchaseOrderItemId, null);
            }
        },
	},
}