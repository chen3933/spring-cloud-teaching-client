import config from "@/config"
import http from "@/utils/request"
export default {
	productType: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/productType/list`,
			name: "获取产品分类",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/productType/save`,
			name: "保存产品分类",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/productType/delete`,
            name: "删除产品分类",
            delete: async function(productTypeId){
                return await http.delete(this.url+"/"+productTypeId, null);
            }
        },
	},
}