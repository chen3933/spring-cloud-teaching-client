import config from "@/config"
import http from "@/utils/request"
export default {
	productGgUnit: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/productGgUnit/list`,
			name: "获取最多5层， 1箱,每箱20袋， 每袋10包，每包10小包  产品采购时，自动保存。下次采购时，界面可以自动联想导入。",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/productGgUnit/save`,
			name: "保存最多5层， 1箱,每箱20袋， 每袋10包，每包10小包  产品采购时，自动保存。下次采购时，界面可以自动联想导入。",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/productGgUnit/delete`,
            name: "删除最多5层， 1箱,每箱20袋， 每袋10包，每包10小包  产品采购时，自动保存。下次采购时，界面可以自动联想导入。",
            delete: async function(productGgUnitId){
                return await http.delete(this.url+"/"+productGgUnitId, null);
            }
        },
	},
}