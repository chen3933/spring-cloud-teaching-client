import config from "@/config"
import http from "@/utils/request"
export default {
	supplierBrand: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierBrand/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierBrand/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/supplierBrand/delete`,
            name: "删除",
            delete: async function(supplierBrandId){
                return await http.delete(this.url+"/"+supplierBrandId, null);
            }
        },
	},
}