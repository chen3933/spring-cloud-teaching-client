import config from "@/config"
import http from "@/utils/request"
export default {
	supplierPrice: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierPrice/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/supplierPrice/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/supplierPrice/delete`,
            name: "删除",
            delete: async function(supplierPriceId){
                return await http.delete(this.url+"/"+supplierPriceId, null);
            }
        },
	},
}