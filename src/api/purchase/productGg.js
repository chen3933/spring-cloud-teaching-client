import config from "@/config"
import http from "@/utils/request"
export default {
	productGg: {
		list: {
			url: `${config.API_URL_PURCHASE}/purchase/productGg/list`,
			name: "获取产品规格",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PURCHASE}/purchase/productGg/save`,
			name: "保存产品规格",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PURCHASE}/purchase/productGg/delete`,
            name: "删除产品规格",
            delete: async function(productGgId){
                return await http.delete(this.url+"/"+productGgId, null);
            }
        },
	},
}