import config from "@/config"
import http from "@/utils/request"
export default {
	cmsContent: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsContent/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsContent/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsContent/delete`,
            name: "删除",
            delete: async function(cmsContentId){
                return await http.delete(this.url+"/"+cmsContentId, null);
            }
        },
	},
}