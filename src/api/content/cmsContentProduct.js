import config from "@/config"
import http from "@/utils/request"
export default {
	cmsContentProduct: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsContentProduct/list`,
			name: "获取内容关联电商产品推荐",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsContentProduct/save`,
			name: "保存内容关联电商产品推荐",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsContentProduct/delete`,
            name: "删除内容关联电商产品推荐",
            delete: async function(cmsContentProductId){
                return await http.delete(this.url+"/"+cmsContentProductId, null);
            }
        },
	},
}