import config from "@/config"
import http from "@/utils/request"
export default {
	cmsContentComment: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsContentComment/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsContentComment/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsContentComment/delete`,
            name: "删除",
            delete: async function(cmsContentCommentId){
                return await http.delete(this.url+"/"+cmsContentCommentId, null);
            }
        },
	},
}