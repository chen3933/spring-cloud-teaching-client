import config from "@/config"
import http from "@/utils/request"
export default {
	cmsType: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsType/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsType/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsType/delete`,
            name: "删除",
            delete: async function(cmsTypeId){
                return await http.delete(this.url+"/"+cmsTypeId, null);
            }
        },
	},
}