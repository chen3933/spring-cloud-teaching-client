import config from "@/config"
import http from "@/utils/request"
export default {
	cmsContentMedia: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsContentMedia/list`,
			name: "获取内容媒体库",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsContentMedia/save`,
			name: "保存内容媒体库",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsContentMedia/delete`,
            name: "删除内容媒体库",
            delete: async function(cmsContentMediaId){
                return await http.delete(this.url+"/"+cmsContentMediaId, null);
            }
        },
	},
}