import config from "@/config"
import http from "@/utils/request"
export default {
	cmsContentContent: {
		list: {
			url: `${config.API_URL_CONTENT}/content/cmsContentContent/list`,
			name: "获取内容关联电商产品推荐",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_CONTENT}/content/cmsContentContent/save`,
			name: "保存内容关联电商产品推荐",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_CONTENT}/content/cmsContentContent/delete`,
            name: "删除内容关联电商产品推荐",
            delete: async function(cmsContentContentId){
                return await http.delete(this.url+"/"+cmsContentContentId, null);
            }
        },
	},
}