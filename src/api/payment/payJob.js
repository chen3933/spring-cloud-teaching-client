import config from "@/config"
import http from "@/utils/request"
export default {
	payJob: {
		list: {
			url: `${config.API_URL_PAYMENT}/payment/payJob/list`,
			name: "获取缓存到mongoDB",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PAYMENT}/payment/payJob/save`,
			name: "保存缓存到mongoDB",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PAYMENT}/payment/payJob/delete`,
            name: "删除缓存到mongoDB",
            delete: async function(payJobId){
                return await http.delete(this.url+"/"+payJobId, null);
            }
        },
	},
}