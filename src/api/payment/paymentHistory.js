import config from "@/config"
import http from "@/utils/request"
export default {
	paymentHistory: {
		list: {
			url: `${config.API_URL_PAYMENT}/payment/paymentHistory/list`,
			name: "获取在线支付历史记录,非在线支付，不会保存到该模块",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PAYMENT}/payment/paymentHistory/save`,
			name: "保存在线支付历史记录,非在线支付，不会保存到该模块",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PAYMENT}/payment/paymentHistory/delete`,
            name: "删除在线支付历史记录,非在线支付，不会保存到该模块",
            delete: async function(paymentHistoryId){
                return await http.delete(this.url+"/"+paymentHistoryId, null);
            }
        },
	},
}