import config from "@/config"
import http from "@/utils/request"
export default {
	paymentMethod: {
		list: {
			url: `${config.API_URL_PAYMENT}/payment/paymentMethod/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PAYMENT}/payment/paymentMethod/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PAYMENT}/payment/paymentMethod/delete`,
            name: "删除",
            delete: async function(paymentMethodId){
                return await http.delete(this.url+"/"+paymentMethodId, null);
            }
        },
	},
}