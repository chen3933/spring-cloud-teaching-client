import config from "@/config"
import http from "@/utils/request"
export default {
	defineNodeUser: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/defineNodeUser/list`,
			name: "获取多人执行关联表 &&  ||",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/defineNodeUser/save`,
			name: "保存多人执行关联表 &&  ||",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/defineNodeUser/delete`,
            name: "删除多人执行关联表 &&  ||",
            delete: async function(defineNodeUserId){
                return await http.delete(this.url+"/"+defineNodeUserId, null);
            }
        },
	},
}