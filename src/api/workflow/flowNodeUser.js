import config from "@/config"
import http from "@/utils/request"
export default {
	flowNodeUser: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeUser/list`,
			name: "获取多人执行关联表 &&  ||",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeUser/save`,
			name: "保存多人执行关联表 &&  ||",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/flowNodeUser/delete`,
            name: "删除多人执行关联表 &&  ||",
            delete: async function(flowNodeUserId){
                return await http.delete(this.url+"/"+flowNodeUserId, null);
            }
        },
	},
}