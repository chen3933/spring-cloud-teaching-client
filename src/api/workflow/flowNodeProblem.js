import config from "@/config"
import http from "@/utils/request"
export default {
	flowNodeProblem: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeProblem/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeProblem/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/flowNodeProblem/delete`,
            name: "删除",
            delete: async function(flowNodeProblemId){
                return await http.delete(this.url+"/"+flowNodeProblemId, null);
            }
        },
	},
}