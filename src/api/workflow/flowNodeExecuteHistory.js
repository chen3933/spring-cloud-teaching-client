import config from "@/config"
import http from "@/utils/request"
export default {
	flowNodeExecuteHistory: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecuteHistory/list`,
			name: "获取工作流节点定义",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecuteHistory/save`,
			name: "保存工作流节点定义",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecuteHistory/delete`,
            name: "删除工作流节点定义",
            delete: async function(flowNodeExecuteHistoryId){
                return await http.delete(this.url+"/"+flowNodeExecuteHistoryId, null);
            }
        },
	},
}