import config from "@/config"
import http from "@/utils/request"
export default {
	flow: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/flow/list`,
			name: "获取工作流定义表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/flow/save`,
			name: "保存工作流定义表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/flow/delete`,
            name: "删除工作流定义表",
            delete: async function(flowId){
                return await http.delete(this.url+"/"+flowId, null);
            }
        },
	},
}