import config from "@/config"
import http from "@/utils/request"
export default {
	flowNodeExecute: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecute/list`,
			name: "获取流程节点执行",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecute/save`,
			name: "保存流程节点执行",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/flowNodeExecute/delete`,
            name: "删除流程节点执行",
            delete: async function(flowNodeExecuteId){
                return await http.delete(this.url+"/"+flowNodeExecuteId, null);
            }
        },
	},
}