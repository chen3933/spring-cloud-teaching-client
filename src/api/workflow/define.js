import config from "@/config"
import http from "@/utils/request"
export default {
	define: {
		list: {
			url: `${config.API_URL_WORKFLOW}/workflow/define/list`,
			name: "获取工作流定义表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_WORKFLOW}/workflow/define/save`,
			name: "保存工作流定义表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_WORKFLOW}/workflow/define/delete`,
            name: "删除工作流定义表",
            delete: async function(defineId){
                return await http.delete(this.url+"/"+defineId, null);
            }
        },
	},
}