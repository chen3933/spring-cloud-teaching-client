import config from "@/config"
import http from "@/utils/request"
export default {
	category: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/category/list`,
			name: "获取系统",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/category/save`,
			name: "保存系统",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/category/delete`,
            name: "删除系统",
            delete: async function(categoryId){
                return await http.delete(this.url+"/"+categoryId, null);
            }
        },
	},
}