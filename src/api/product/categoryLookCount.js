import config from "@/config"
import http from "@/utils/request"
export default {
	categoryLookCount: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/categoryLookCount/list`,
			name: "获取作业调度，按天统计，在mongodb备份详情",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/categoryLookCount/save`,
			name: "保存作业调度，按天统计，在mongodb备份详情",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/categoryLookCount/delete`,
            name: "删除作业调度，按天统计，在mongodb备份详情",
            delete: async function(categoryLookCountId){
                return await http.delete(this.url+"/"+categoryLookCountId, null);
            }
        },
	},
}