import config from "@/config"
import http from "@/utils/request"
export default {
	productCode: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productCode/list`,
			name: "获取产品规格条形码",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productCode/save`,
			name: "保存产品规格条形码",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productCode/delete`,
            name: "删除产品规格条形码",
            delete: async function(productCodeId){
                return await http.delete(this.url+"/"+productCodeId, null);
            }
        },
	},
}