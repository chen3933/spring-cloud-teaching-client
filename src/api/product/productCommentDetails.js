import config from "@/config"
import http from "@/utils/request"
export default {
	productCommentDetails: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productCommentDetails/list`,
			name: "获取产品评论表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productCommentDetails/save`,
			name: "保存产品评论表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productCommentDetails/delete`,
            name: "删除产品评论表",
            delete: async function(productCommentDetailsId){
                return await http.delete(this.url+"/"+productCommentDetailsId, null);
            }
        },
	},
}