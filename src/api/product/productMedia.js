import config from "@/config"
import http from "@/utils/request"
export default {
	productMedia: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productMedia/list`,
			name: "获取产品图片",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productMedia/save`,
			name: "保存产品图片",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productMedia/delete`,
            name: "删除产品图片",
            delete: async function(productMediaId){
                return await http.delete(this.url+"/"+productMediaId, null);
            }
        },
	},
}