import config from "@/config"
import http from "@/utils/request"
export default {
	categoryProduct: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/categoryProduct/list`,
			name: "获取目录产品",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/categoryProduct/save`,
			name: "保存目录产品",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/categoryProduct/delete`,
            name: "删除目录产品",
            delete: async function(categoryProductId){
                return await http.delete(this.url+"/"+categoryProductId, null);
            }
        },
	},
}