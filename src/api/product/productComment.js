import config from "@/config"
import http from "@/utils/request"
export default {
	productComment: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productComment/list`,
			name: "获取产品评论表",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productComment/save`,
			name: "保存产品评论表",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productComment/delete`,
            name: "删除产品评论表",
            delete: async function(productCommentId){
                return await http.delete(this.url+"/"+productCommentId, null);
            }
        },
	},
}