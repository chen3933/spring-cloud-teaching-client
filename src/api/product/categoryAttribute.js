import config from "@/config"
import http from "@/utils/request"
export default {
	categoryAttribute: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/categoryAttribute/list`,
			name: "获取栏目属性",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/categoryAttribute/save`,
			name: "保存栏目属性",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/categoryAttribute/delete`,
            name: "删除栏目属性",
            delete: async function(categoryAttributeId){
                return await http.delete(this.url+"/"+categoryAttributeId, null);
            }
        },
	},
}