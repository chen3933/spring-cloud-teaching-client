import config from "@/config"
import http from "@/utils/request"
export default {
	productDetails: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productDetails/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productDetails/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productDetails/delete`,
            name: "删除",
            delete: async function(productDetailsId){
                return await http.delete(this.url+"/"+productDetailsId, null);
            }
        },
	},
}