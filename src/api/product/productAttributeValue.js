import config from "@/config"
import http from "@/utils/request"
export default {
	productAttributeValue: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productAttributeValue/list`,
			name: "获取产品属性值",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productAttributeValue/save`,
			name: "保存产品属性值",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productAttributeValue/delete`,
            name: "删除产品属性值",
            delete: async function(productAttributeValueId){
                return await http.delete(this.url+"/"+productAttributeValueId, null);
            }
        },
	},
}