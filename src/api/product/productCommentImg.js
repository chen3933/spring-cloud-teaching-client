import config from "@/config"
import http from "@/utils/request"
export default {
	productCommentImg: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productCommentImg/list`,
			name: "获取评论媒体",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productCommentImg/save`,
			name: "保存评论媒体",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productCommentImg/delete`,
            name: "删除评论媒体",
            delete: async function(productCommentImgId){
                return await http.delete(this.url+"/"+productCommentImgId, null);
            }
        },
	},
}