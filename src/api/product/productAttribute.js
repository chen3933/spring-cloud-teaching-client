import config from "@/config"
import http from "@/utils/request"
export default {
	productAttribute: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productAttribute/list`,
			name: "获取产品属性",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productAttribute/save`,
			name: "保存产品属性",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productAttribute/delete`,
            name: "删除产品属性",
            delete: async function(productAttributeId){
                return await http.delete(this.url+"/"+productAttributeId, null);
            }
        },
	},
}