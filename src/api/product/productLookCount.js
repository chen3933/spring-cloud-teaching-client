import config from "@/config"
import http from "@/utils/request"
export default {
	productLookCount: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productLookCount/list`,
			name: "获取按天统计，mongodb备份",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productLookCount/save`,
			name: "保存按天统计，mongodb备份",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productLookCount/delete`,
            name: "删除按天统计，mongodb备份",
            delete: async function(productLookCountId){
                return await http.delete(this.url+"/"+productLookCountId, null);
            }
        },
	},
}