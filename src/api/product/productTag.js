import config from "@/config"
import http from "@/utils/request"
export default {
	productTag: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/productTag/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/productTag/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/productTag/delete`,
            name: "删除",
            delete: async function(productTagId){
                return await http.delete(this.url+"/"+productTagId, null);
            }
        },
	},
}