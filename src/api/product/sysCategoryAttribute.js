import config from "@/config"
import http from "@/utils/request"
export default {
	sysCategoryAttribute: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/sysCategoryAttribute/list`,
			name: "获取系统默认属性",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/sysCategoryAttribute/save`,
			name: "保存系统默认属性",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/sysCategoryAttribute/delete`,
            name: "删除系统默认属性",
            delete: async function(sysCategoryAttributeId){
                return await http.delete(this.url+"/"+sysCategoryAttributeId, null);
            }
        },
	},
}