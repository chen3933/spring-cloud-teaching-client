import config from "@/config"
import http from "@/utils/request"
export default {
	sysCategory: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/sysCategory/list`,
			name: "获取系统",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/sysCategory/save`,
			name: "保存系统",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/sysCategory/delete`,
            name: "删除系统",
            delete: async function(sysCategoryId){
                return await http.delete(this.url+"/"+sysCategoryId, null);
            }
        },
	},
}