import config from "@/config"
import http from "@/utils/request"
export default {
	series: {
		list: {
			url: `${config.API_URL_PRODUCT}/product/series/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PRODUCT}/product/series/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PRODUCT}/product/series/delete`,
            name: "删除",
            delete: async function(seriesId){
                return await http.delete(this.url+"/"+seriesId, null);
            }
        },
	},
}