import config from "@/config"
import http from "@/utils/request"
export default {
	testUser: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/testUser/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/testUser/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/testUser/delete`,
            name: "删除",
            delete: async function(testUserId){
                return await http.delete(this.url+"/"+testUserId, null);
            }
        },
	},
}