import config from "@/config"
import http from "@/utils/request"
export default {
	sysDepartment: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysDepartment/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysDepartment/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysDepartment/delete`,
            name: "删除",
            delete: async function(sysDepartmentId){
                return await http.delete(this.url+"/"+sysDepartmentId, null);
            }
        },
	},
}