import config from "@/config"
import http from "@/utils/request"
export default {
	sysParameter: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysParameter/list`,
			name: "获取参数配置",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysParameter/save`,
			name: "保存参数配置",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysParameter/delete`,
            name: "删除参数配置",
            delete: async function(sysParameterId){
                return await http.delete(this.url+"/"+sysParameterId, null);
            }
        },
	},
}