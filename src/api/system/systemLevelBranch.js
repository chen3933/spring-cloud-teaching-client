import config from "@/config"
import http from "@/utils/request"
export default {
	systemLevelBranch: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/systemLevelBranch/list`,
			name: "获取一个分店 只能有一种系统",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/systemLevelBranch/save`,
			name: "保存一个分店 只能有一种系统",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/systemLevelBranch/delete`,
            name: "删除一个分店 只能有一种系统",
            delete: async function(systemLevelBranchId){
                return await http.delete(this.url+"/"+systemLevelBranchId, null);
            }
        },
	},
}