import config from "@/config"
import http from "@/utils/request"
export default {
	systemLevel: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/systemLevel/list`,
			name: "获取系统",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		initlist: {
			url: `${config.API_URL_SYSTEM}/system/systemLevel/initlist`,
			name: "获取系统",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/systemLevel/save`,
			name: "保存系统",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/systemLevel/delete`,
            name: "删除系统",
            delete: async function(systemLevelId){
                return await http.delete(this.url+"/"+systemLevelId, null);
            }
        },
	},
}