import config from "@/config"
import http from "@/utils/request"
export default {
	sysCompany: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysCompany/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		init: {
			url: `${config.API_URL_SYSTEM}/system/sysCompany/init`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysCompany/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysCompany/delete`,
            name: "删除",
            delete: async function(sysCompanyId){
                return await http.delete(this.url+"/"+sysCompanyId, null);
            }
        },
		getById: {
		    url: `${config.API_URL_SYSTEM}/system/sysCompany/getById`,
		    name: "获取对象",
		    getById: async function(sysCompanyId){
		        return await http.get(this.url+"/"+sysCompanyId, null);
		    }
		},
	},
}