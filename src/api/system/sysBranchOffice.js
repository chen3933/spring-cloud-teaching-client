import config from "@/config"
import http from "@/utils/request"
export default {
	sysBranchOffice: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysBranchOffice/list`,
			name: "获取分公司 分店",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysBranchOffice/save`,
			name: "保存分公司 分店",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysBranchOffice/delete`,
            name: "删除分公司 分店",
            delete: async function(sysBranchOfficeId){
                return await http.delete(this.url+"/"+sysBranchOfficeId, null);
            }
        },
	},
}