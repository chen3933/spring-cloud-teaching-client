import config from "@/config"
import http from "@/utils/request"
export default {
	sysRegion: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysRegion/list`,
			name: "获取地理位置设置",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		listSelect: {
			url: `${config.API_URL_SYSTEM}/system/sysRegion/listSelect`,
			name: "获取地理位置设置",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysRegion/save`,
			name: "保存地理位置设置",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysRegion/delete`,
            name: "删除地理位置设置",
            delete: async function(sysRegionId){
                return await http.delete(this.url+"/"+sysRegionId, null);
            }
        },
	},
}