import config from "@/config"
import http from "@/utils/request"
export default {
	systemLevelType: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/systemLevelType/list`,
			name: "获取系统等级分类",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/systemLevelType/save`,
			name: "保存系统等级分类",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/systemLevelType/delete`,
            name: "删除系统等级分类",
            delete: async function(systemLevelTypeId){
                return await http.delete(this.url+"/"+systemLevelTypeId, null);
            }
        },
	},
}