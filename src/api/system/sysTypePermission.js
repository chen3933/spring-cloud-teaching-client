import config from "@/config"
import http from "@/utils/request"
export default {
	sysTypePermission: {
		list: {
			url: `${config.API_URL_SYSTEM}/system/sysTypePermission/list`,
			name: "获取系统等级资源--系统权限",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SYSTEM}/system/sysTypePermission/save`,
			name: "保存系统等级资源--系统权限",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SYSTEM}/system/sysTypePermission/delete`,
            name: "删除系统等级资源--系统权限",
            delete: async function(sysTypePermissionId){
                return await http.delete(this.url+"/"+sysTypePermissionId, null);
            }
        },
	},
}