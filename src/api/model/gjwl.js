import config from "@/config"
import http from "@/utils/request"
export default {
	wlGoods: {
			list: {
				url: `${config.API_URL_GJWL}/gjwl/wlGoods/list`,
				name: "获取",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
			save: {
				url: `${config.API_URL_GJWL}/gjwl/wlGoods/save`,
				name: "保存",
				post: async function(data={}){
					return await http.post(this.url, data);
				}
			},
	        delete: {
	            url: `${config.API_URL_GJWL}/gjwl/wlGoods/delete`,
	            name: "删除",
	            delete: async function(wlGoodsId){
	                return await http.delete(this.url+"/"+wlGoodsId, null);
	            }
	        },
		},
		testStudent: {
				list: {
					url: `${config.API_URL_GJWL}/gjwl/testStudent/list`,
					name: "获取学生",
					get: async function(params){
						return await http.get(this.url,params);
					}
				},
				save: {
					url: `${config.API_URL_GJWL}/gjwl/testStudent/save`,
					name: "保存学生",
					post: async function(data={}){
						return await http.post(this.url, data);
					}
				},
		        delete: {
		            url: `${config.API_URL_GJWL}/gjwl/testStudent/delete`,
		            name: "删除学生",
		            delete: async function(testStudentId){
		                return await http.delete(this.url+"/"+testStudentId, null);
		            }
		        },
			},
	wlWarehouse: {
			list: {
				url: `${config.API_URL_GJWL}/gjwl/wlWarehouse/list`,
				name: "获取",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
			save: {
				url: `${config.API_URL_GJWL}/gjwl/wlWarehouse/save`,
				name: "保存",
				post: async function(data={}){
					return await http.post(this.url, data);
				}
			},
	        delete: {
	            url: `${config.API_URL_GJWL}/gjwl/wlWarehouse/delete`,
	            name: "删除",
	            delete: async function(wlWarehouseId){
	                return await http.delete(this.url+"/"+wlWarehouseId, null);
	            }
	        },
			listAll:{
				url: `${config.API_URL_GJWL}/gjwl/wlWarehouse/listAll`,
				name: "获取全部",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
		},
			
}
