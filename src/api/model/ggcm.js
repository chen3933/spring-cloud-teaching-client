import config from "@/config"
import http from "@/utils/request"

export default {
	testUser: {
		list: {
			url: `${config.API_URL_GGCM}/testUser/list`,
			name: "获取测试用户",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_GGCM}/testUser/save`,
			name: "保存测试用户",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_GGCM}/testUser/delete`,
            name: "删除测试用户",
            delete: async function(testUserId){
                return await http.delete(this.url+"/"+testUserId, null);
            }
        },
	},
	testStudent: {
		list: {
			url: `${config.API_URL_GGCM}/ggcm/testStudent/list`,
			name: "获取学生",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_GGCM}/ggcm/testStudent/save`,
			name: "保存学生",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
	    delete: {
	        url: `${config.API_URL_GGCM}/ggcm/testStudent/delete`,
	        name: "删除学生",
	        delete: async function(testStudentId){
	            return await http.delete(this.url+"/"+testStudentId, null);
	        }
	    },
	},
	testDepartment: {
			list: {
				url: `${config.API_URL_GGCM}/testDepartment/list`,
				name: "获取",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
			save: {
				url: `${config.API_URL_GGCM}/testDepartment/save`,
				name: "保存",
				post: async function(data={}){
					return await http.post(this.url, data);
				}
			},
	        delete: {
	            url: `${config.API_URL_GGCM}/testDepartment/delete`,
	            name: "删除",
	            delete: async function(testDepartmentId){
	                return await http.delete(this.url+"/"+testDepartmentId, null);
	            }
	        },
		},
		ggcmCategory: {
				list: {
					url: `${config.API_URL_GGCM}/ggcm/ggcmCategory/list`,
					name: "获取",
					get: async function(params){
						return await http.get(this.url,params);
					}
				},
				save: {
					url: `${config.API_URL_GGCM}/ggcm/ggcmCategory/save`,
					name: "保存",
					post: async function(data={}){
						return await http.post(this.url, data);
					}
				},
		        delete: {
		            url: `${config.API_URL_GGCM}/ggcm/ggcmCategory/delete`,
		            name: "删除",
		            delete: async function(ggcmCategoryId){
		                return await http.delete(this.url+"/"+ggcmCategoryId, null);
		            }
		        },
			},
	ggcmProduct: {
		list: {
			url: `${config.API_URL_GGCM}/ggcm/ggcmProduct/list`,
			name: "获取",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_GGCM}/ggcm/ggcmProduct/save`,
			name: "保存",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_GGCM}/ggcm/ggcmProduct/delete`,
            name: "删除",
            delete: async function(ggcmProductId){
                return await http.delete(this.url+"/"+ggcmProductId, null);
            }
        },
	},
	ggcmOrder: {
			list: {
				url: `${config.API_URL_GGCM}/ggcm/ggcmOrder/list`,
				name: "获取",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
			save: {
				url: `${config.API_URL_GGCM}/ggcm/ggcmOrder/save`,
				name: "保存",
				post: async function(data={}){
					return await http.post(this.url, data);
				}
			},
	        delete: {
	            url: `${config.API_URL_GGCM}/ggcm/ggcmOrder/delete`,
	            name: "删除",
	            delete: async function(ggcmOrderId){
	                return await http.delete(this.url+"/"+ggcmOrderId, null);
	            }
	        },
		},
}
/**
 *
 一、async/await是什么？
 简而言之， async用于申明一个function是异步的；
 而await则可以认为是 async await的简写形式，是等待一个异步方法执行完成的。

 二、async和await的基础使用
 async/awiat的使用规则:
 async 表示这是一个async函数， await只能用在async函数里面，不能单独使用
 async 返回的是一个Promise对象，await就是等待这个promise的返回结果后，再继续执行
 await 等待的是一个Promise对象，后面必须跟一个Promise对象，但是不必写then()，直接就可以得到返回值

 三、async/await的特点
 1、Async作为关键字放在函数前面，普通函数变成了异步函数
 2、异步函数async函数调用，跟普通函数调用方式一样。在一个函数前面加上async，变成 async函数，异步函数，return:1，打印返回值，
 3、返回的是promise成功的对象，
 4、Async函数配合await关键字使用

 */
