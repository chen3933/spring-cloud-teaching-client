import config from "@/config"
import http from "@/utils/request"
export default {
	dsOrderSales: {
		list: {
			url: `${config.API_URL_ORDER}/order/dsOrderSales/list`,
			name: "获取购物订单",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_ORDER}/order/dsOrderSales/save`,
			name: "保存购物订单",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_ORDER}/order/dsOrderSales/delete`,
            name: "删除购物订单",
            delete: async function(dsOrderSalesId){
                return await http.delete(this.url+"/"+dsOrderSalesId, null);
            }
        },
	},
}