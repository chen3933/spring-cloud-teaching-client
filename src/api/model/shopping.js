import config from "@/config"
import http from "@/utils/request"
export default {
	dsProduct: {
		list: {
			url: `${config.API_URL_SHOPPING}/shopping/dsProduct/list`,
			name: "获取产品",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_SHOPPING}/shopping/dsProduct/save`,
			name: "保存产品",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_SHOPPING}/shopping/dsProduct/delete`,
            name: "删除产品",
            delete: async function(dsProductId){
                return await http.delete(this.url+"/"+dsProductId, null);
            }
        },
	},
}