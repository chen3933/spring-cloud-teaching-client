import config from "@/config"
import http from "@/utils/request"
export default {
	paymentMethod: {
		list: {
			url: `${config.API_URL_PAY}/pay/paymentMethod/list`,
			name: "获取支付方式",
			get: async function(params){
				return await http.get(this.url,params);
			}
		},
		save: {
			url: `${config.API_URL_PAY}/pay/paymentMethod/save`,
			name: "保存支付方式",
			post: async function(data={}){
				return await http.post(this.url, data);
			}
		},
        delete: {
            url: `${config.API_URL_PAY}/pay/paymentMethod/delete`,
            name: "删除支付方式",
            delete: async function(paymentMethodId){
                return await http.delete(this.url+"/"+paymentMethodId, null);
            }
        },
	},
	paymentHistory: {
			list: {
				url: `${config.API_URL_PAY}/pay/paymentHistory/list`,
				name: "获取支付记录",
				get: async function(params){
					return await http.get(this.url,params);
				}
			},
			save: {
				url: `${config.API_URL_PAY}/pay/paymentHistory/save`,
				name: "保存支付记录",
				post: async function(data={}){
					return await http.post(this.url, data);
				}
			},
	        delete: {
	            url: `${config.API_URL_PAY}/pay/paymentHistory/delete`,
	            name: "删除支付记录",
	            delete: async function(paymentHistoryId){
	                return await http.delete(this.url+"/"+paymentHistoryId, null);
	            }
	        },
		},
}
